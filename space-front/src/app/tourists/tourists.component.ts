import { Component, OnInit, ViewChild } from '@angular/core';
import { TOURISTS } from './tourist-mockup';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-tourists',
  templateUrl: './tourists.component.html',
  styleUrls: ['./tourists.component.css']
})
export class TouristsComponent implements OnInit {
  tourists = TOURISTS;
  dataSource;
  displayedColumns = [];
  @ViewChild(MatSort, {static: true }) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  columnNames = [
    {
      id: "firstName",
      value: "First Name"
  
    }, 
    {
      id: "lastName",
      value: "Last Name"
    },
    {
      id: "gender",
      value: "Gender"
    },
    {
      id: "country",
      value: "Country"
    },
    {
      id: "remarks",
      value: "Remarks"
    },
    {
      id: "dateOfBirth",
      value: "Date of Birth"
    },
  
    {
      id: "action",
      value: "Action"
    },
    ];
  constructor() { }

  ngOnInit() {
    this.displayedColumns = this.columnNames.map(x => x.id);
    this.dataSource = new MatTableDataSource(this.tourists);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
