import { Tourist } from "../Tourist";
import {Gender} from "../Gender";
let t1 = new Tourist(1,'Jan', 'Nowak', Gender.MALE, 'Poland', 'no remarks', 635472000);
let t2 = new Tourist(2,'Bill', 'Gates', Gender.MALE, 'USA', 'no remarks', 635472000);
let t3 = new Tourist(3,'Mark', 'Zukenberg', Gender.MALE, 'Germany', 'no remarks', 635472000);
let t4 = new Tourist(4,'Ivan', 'Kalinin', Gender.MALE, 'Russia', 'no remarks', 635472000);
let t5 = new Tourist(5,'Luka', 'Modrić', Gender.MALE, 'Croatia', 'no remarks', 635472000);
let t6 = new Tourist(6,'Nicholas', 'Nujicic', Gender.MALE, 'Australia', 'no remarks', 635472000);
let t7 = new Tourist(7,'MacKenzie', 'Bezos', Gender.FEMALE, 'USA', 'no remarks', 635472000);
let t8 = new Tourist(8,'Elon', 'Musk', Gender.MALE, 'South Africa', 'no remarks', 635472000);
let t9 = new Tourist(9,'Anna', 'Lewandowska', Gender.FEMALE, 'Poland', 'no remarks', 635472000);
let t10 = new Tourist(10,'Julia', 'Roberts', Gender.FEMALE, 'USA', 'no remarks', 635472000);

export const TOURISTS: Tourist[] = [t1,t2,t3,t4,t5,t6,t7,t8,t9,t10]
