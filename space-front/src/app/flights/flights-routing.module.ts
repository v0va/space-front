import { Routes, RouterModule } from '@angular/router';
import { FlightsComponent } from './flights.component';
import { EditFlightComponent } from './edit-flight/edit-flight.component';
import { AddFlightComponent } from './add-flight/add-flight.component';
import { NgModule } from '@angular/core';

const adminRoutes: Routes = [
    {
      path: '',
      component: FlightsComponent,
      children: [
        {
          path: '',
          children: [
            { path: 'edit', component: EditFlightComponent },
            { path: 'add', component: AddFlightComponent }
          ]
        }
      ]
    }
  ];
  
  @NgModule({
    imports: [
      RouterModule.forChild(adminRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
export class FlightRoutingModule {}