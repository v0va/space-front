import { Component, OnInit } from '@angular/core';
import { Flight } from 'src/app/Flight';
import {TOURISTS} from '../../tourists/tourist-mockup';
import { Http } from '@angular/http';
@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.css']
})
export class AddFlightComponent implements OnInit {

  // tourists = [];  
  // private t = TOURISTS.forEach(element => {
    
  //   this.tourists.push(element.toString());

  // });
  

  model = new Flight(100,'',0,0,0,0);

  submitted = false;

  newDepartureTime = null;
  newArrivalTime = null;

  // TODO: Remove this when we're done
  // get diagnostic() { return JSON.stringify(this.model); }

  newFlight() {
    this.model = new Flight(100,'a',0,0,0,0);
  }

  // skyDog(): Hero {
  //   let myHero =  new Hero(42, 'SkyDog',
  //                          'Fetch any object at any distance',
  //                          'Leslie Rollover');
  //   console.log('My hero is called ' + myHero.name); // "My hero is called SkyDog"
  //   return myHero;
  // }

  //////// NOT SHOWN IN DOCS ////////

  // Reveal in html:
  //   Name via form.controls = {{showFormControls(heroForm)}}
  // showFormControls(form: any) {
  //   return form && form.controls['title'] &&
  //   form.controls['title'].value; // Dr. IQ
  // }

  constructor(private http:Http) { }

  ngOnInit() {
  }
  
  updateDate(date:string, varToSign:string){
    let d = new Date();
    console.log("date: " + date);
    let year = Number(date.substring(0,4));
    let month = Number(date.substring(5,7));
    let day = Number(date.substring(8,10));
    let hour = Number(date.substring(11,13));
    let minute = Number(date.substring(14,16));
    
    d.setFullYear(year, month-1, day);
    d.setHours(hour, minute);
    //console.log("dateInMilisec: " + d.toString());
    if(varToSign =='departureTime')
      this.newDepartureTime = d;
    if(varToSign == 'arrivalTime')
      this.newArrivalTime = d;
  }

  onSubmit() { this.submitted = true; 

    if(this.newDepartureTime){this.model.departureTime = this.newDepartureTime.getTime()/1000;this.newDepartureTime=null;}
    if(this.newArrivalTime){this.model.arrivalTime = this.newArrivalTime.getTime()/1000;this.newArrivalTime=null;}

    console.log("JSON: \n" +JSON.stringify(this.model));
    
  }

}
