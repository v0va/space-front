

export function fromUnixTimeStampToDate(timestamp:number){
    return new Date(timestamp);
}

export function fromDateToUnixTimeStamp(date: Date){
    return date.getTime();
}