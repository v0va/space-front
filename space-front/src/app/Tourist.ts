import { Flight } from './Flight';
import { Gender } from './Gender';

export class Tourist{
    id : number;
    firstName : string;
    lastName : string;
    gender: Gender;
    country : string;
    remarks : string;
    dateOfBirth : number;
    listOfFlights : Flight[];

    constructor(id:number,firstName: string, lastName:string, gender:Gender, country:string, remarks:string,dateOfBirth:number){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.country = country;
        this.remarks = remarks;
        this.dateOfBirth = dateOfBirth;
    }

    toString(){
        return this.firstName + ' ' + this.lastName;
    }
}