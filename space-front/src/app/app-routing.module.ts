import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlightsComponent } from './flights/flights.component';
import { EditFlightComponent } from './flights/edit-flight/edit-flight.component';
import { AppComponent } from './app.component';
import { AddFlightComponent } from './flights/add-flight/add-flight.component';
import { EditTouristComponent } from './tourists/edit-tourist/edit-tourist.component';
import { AddTouristComponent } from './tourists/add-tourist/add-tourist.component';
import { AboutComponent } from './about/about.component';
import { TouristsComponent } from './tourists/tourists.component';

const appRoutes: Routes = [

  { path: 'about',
  component: AboutComponent
  },
  { path: 'tourists/add',
  component: AddTouristComponent
  },

  { path: 'tourists/edit/:id',
  component: EditTouristComponent
  },
  {
    path: 'tourists',
    component:TouristsComponent,
  },
  { path: 'flights/add',
  component: AddFlightComponent
  },

  { path: 'flights/edit/:id',
  component: EditFlightComponent
  },
  {
    path: 'flights',
    component:FlightsComponent,
  },
  {path: 'home', redirectTo: '', pathMatch: 'full'},
  {path: '', redirectTo: '/about', pathMatch: 'full'},
  { path: '*', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,
    // { enableTracing: true } // <-- debugging purposes only
  )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
